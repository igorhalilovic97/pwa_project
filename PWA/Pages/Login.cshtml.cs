using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PWA.Data;
using System.ComponentModel.DataAnnotations;

namespace PWA.Pages
{
    public class LoginModel : PageModel
    {
        private readonly SignInManager<User> _signInManager;

        [BindProperty]
        [Required(ErrorMessage = "Field required")] 
        [EmailAddress(ErrorMessage = "Field invalid")]
        public string Email { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Field required")]
        [DataType(DataType.Password, ErrorMessage = "Field required")]
        public string Password { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public string? ReturnUrl { get; set; }

        public LoginModel(SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
        }

        public async Task OnGet(string? returnUrl = null)
        {
            ReturnUrl = returnUrl;

            if (!string.IsNullOrEmpty(ErrorMessage))
                ModelState.AddModelError(string.Empty, ErrorMessage);

            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

        }

        public async Task<IActionResult> OnPost(string? returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(Email, Password, true, false);
                if (result.Succeeded)
                {
                    var id = (await _signInManager.UserManager.FindByEmailAsync(Email))!.Id;

                    return LocalRedirect(returnUrl ?? Url.Page("/Index") ?? "/");
                }
                else if (result.IsNotAllowed)
                {
                    ModelState.AddModelError(string.Empty, "Not allowed to log in.");
                }
                else if (result.IsLockedOut)
                {
                    ModelState.AddModelError(string.Empty, "Locked out");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt");
                }
            }

            return Page();
        }
    }
}
