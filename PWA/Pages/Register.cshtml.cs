using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PWA.Data;

namespace PWA.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly ILogger<RegisterModel> _logger;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string FirstName { get; set; }

        [BindProperty]
        public string LastName { get; set; }

        [BindProperty]
        public string? Phone { get; set; }

        [BindProperty]
        public string Password { get; set; }

        [BindProperty]
        public string ConfirmPassword { get; set; }

        public string? ReturnUrl { get; set; }

        public RegisterModel(ILogger<RegisterModel> logger, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public void OnGet(string? returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var user = new User(Email)
                {
                    FirstName = FirstName,
                    LastName = LastName,
                    PhoneNumber = Phone,
                };
                user.EmailConfirmed = true;


                var result = await _userManager.CreateAsync(user, Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation($"User {user.Id} created with email and password.");
                }
                if (result.Errors.Any())
                {
                    ModelState.AddModelError(string.Empty, "Error");
                }
            }

            return Page();
        }
    }
}
