using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using PWA;
using PWA.Data;
using System.Reflection;

public class Program
{
    public static string Version { get; } = Assembly.GetExecutingAssembly().GetCustomAttributes<AssemblyInformationalVersionAttribute>().First().InformationalVersion;

    private static WebApplicationBuilder GetBuilder()
        => WebApplication.CreateBuilder(new WebApplicationOptions()
        {
            WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "Content")
        });

    private static void AddConfiguration(WebApplicationBuilder builder)
    {
        builder.Configuration
            .Sources.Clear();

        builder.Configuration
            .AddJsonFile("Settings.json", false, true)
            .AddJsonFile("Settings.Development.json", true, true)
            .AddUserSecrets<Program>()
            .AddEnvironmentVariables("ASPNETCORE_")
            .AddEnvironmentVariables("DOTNET_");
    }

    private static void AddDbContext(WebApplicationBuilder builder)
    {
        builder.Services
            .AddDbContext<DataContext>(options =>
            {
                options.UseMySql(
                    builder.Configuration.GetConnectionString("Default"),
                    new MySqlServerVersion(new Version(8, 0)),
                    mySqlOptions =>
                    {
                        mySqlOptions.MigrationsHistoryTable("_migrations", DataContext.Schema);
                    });
            });
    }

    private static void AddIdentity(WebApplicationBuilder builder)
    {
        builder.Services
            .AddAuthentication(o =>
            {
                o.DefaultScheme = IdentityConstants.ApplicationScheme;
                o.DefaultSignInScheme = IdentityConstants.ExternalScheme;
            })
            .AddIdentityCookies(options =>
            {
                options.ApplicationCookie!.Configure(o =>
                {
                    o.LoginPath = "/login";
                    o.LogoutPath = "/logout";
                    o.AccessDeniedPath = "/error/403";
                    o.SlidingExpiration = true;

                    o.Cookie.Name = "a_in";
                    o.Cookie.HttpOnly = true;
                });

                options.ExternalCookie!.Configure(o =>
                {
                    o.LoginPath = "/login";
                    o.LogoutPath = "/logout";
                    o.AccessDeniedPath = "/error/403";
                    o.SlidingExpiration = true;

                    o.Cookie.Name = "a_ex";
                    o.Cookie.HttpOnly = true;
                    o.Cookie.SameSite = SameSiteMode.Lax;
                });
            });

        builder.Services.
            AddIdentityCore<User>(o =>
            {
                if (!builder.Environment.IsProduction())
                {
                    o.Stores.MaxLengthForKeys = 128;

                    o.Password.RequireNonAlphanumeric = false;
                    o.Password.RequiredLength = 6;
                    o.Password.RequireDigit = false;
                    o.Password.RequiredUniqueChars = 0;
                    o.Password.RequireUppercase = false;
                    o.Password.RequireLowercase = false;

                    o.SignIn.RequireConfirmedAccount = true;
                }
            })
            .AddSignInManager()
            .AddRoles<Role>()
            .AddDefaultTokenProviders()
            .AddEntityFrameworkStores<DataContext>();
    }

    private static void AddPagesAndControllers(WebApplicationBuilder builder)
    {
        builder.Services
            .AddAuthorization(o =>
            {
                o.AddPolicy(Constants.AuthorizationPolicies.AdminOnly, p =>
                    p.RequireRole(Constants.Roles.Administrator));
            }).AddRazorPages(o =>
                o.Conventions
                    .AuthorizeFolder("/Profile")
                    .AuthorizeFolder("/Admin", Constants.AuthorizationPolicies.AdminOnly));

        builder.Services.AddControllers();
    }

     private static void ConfigureServices(WebApplicationBuilder builder)
    {
        builder.Services
            .Configure<RazorViewEngineOptions>(o =>
            {
                o.ViewLocationFormats.Add("/Views/Templates/{0}" + RazorViewEngine.ViewExtension);
            })
            .Configure<CookieTempDataProviderOptions>(o =>
            {
                o.Cookie.Name = "tdt";
            })
            .Configure<AntiforgeryOptions>(o =>
            {
                o.Cookie.Name = "af";
            });
    }

    private static void UseProxyHeaders(WebApplication app)
    {
        if (app.Configuration.GetValue<bool>("UseProxyHeaders"))
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
    }

    private static void UsePagesAndControllers(WebApplication app)
    {
        app.UseAuthentication();
        app.UseAuthorization();
        app.MapRazorPages();
        app.MapControllers();
    }

    public static void Main(string[] args)
    {
        var builder = GetBuilder();

        AddConfiguration(builder);
        AddDbContext(builder);
        AddIdentity(builder);
        AddPagesAndControllers(builder);
        ConfigureServices(builder);

        var app = builder.Build();

        app.UseExceptionHandler("/error/1");
        app.UseStatusCodePagesWithReExecute("/error/{0}");

        if (app.Environment.IsProduction())
            app.UseHsts();

        app.UseStaticFiles();
        app.UseRouting();

        UseProxyHeaders(app);
        UsePagesAndControllers(app);

        app.Run();
    }
}
