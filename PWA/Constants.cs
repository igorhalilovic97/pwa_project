﻿namespace PWA
{
    public class Constants
    {
        public static class AuthorizationPolicies
        {
            public const string
                AdminOnly = "AdminOnly";
        }

        public static class Roles
        {
            public const string
                Administrator = "Administrator",
                AdministratorLower = "administrator",
                Customer = "Customer",
                CustomerLower = "customer";
        }
    }
}
