﻿using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Reflection;

namespace PWA.Data;

public class News
{
    public int Id { get; set; }
    [StringLength(30,MinimumLength =5)]
    [Required]
    public string Title { get; set; }
    [Required]
    public string Text { get; set; }
    [StringLength(100,MinimumLength = 10)]
    [Required]
    public string Brief { get; set; }
    [Required]
    public string Category { get; set; }
    public bool Archive { get; set; }
    [Required]
    public DateTime Date { get; set; }
    //[Required]
    //public Image? Image { get; set; }
}
