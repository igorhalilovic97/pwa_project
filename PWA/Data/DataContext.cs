﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Azure;
using static Azure.Core.HttpHeader;

namespace PWA.Data;

public class DataContext : IdentityDbContext<User, Role, Guid>
{
    public const string Schema = null;

    public DbSet<News> News { get; set; }
    public DbSet<User> User { get; set; }


    public DataContext(DbContextOptions options)
       : base(options)
    { }

    private static void Configure(EntityTypeBuilder<User> builder)
    {
        builder.ToTable("users");

    }
    private static void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.ToTable("roles");
    }

    private static void Configure(EntityTypeBuilder<IdentityUserRole<Guid>> builder)
    {
        builder.ToTable("user_roles");
    }

    private static void Configure(EntityTypeBuilder<IdentityRoleClaim<Guid>> builder)
    {
        builder.ToTable("role_claims");
    }

    private static void Configure(EntityTypeBuilder<IdentityUserLogin<Guid>> builder)
    {
        builder.ToTable("user_logins");
    }

    private static void Configure(EntityTypeBuilder<IdentityUserToken<Guid>> builder)
    {
        builder.ToTable("user_tokens");
    }

    private static void Configure(EntityTypeBuilder<IdentityUserClaim<Guid>> builder)
    {
        builder.ToTable("user_claims");
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.HasCharSet("Utf8mb4");
        builder.HasDefaultSchema(Schema);

        base.OnModelCreating(builder);

        Configure(builder.Entity<User>());
        Configure(builder.Entity<Role>());
        Configure(builder.Entity<IdentityRoleClaim<Guid>>());
        Configure(builder.Entity<IdentityUserRole<Guid>>());
        Configure(builder.Entity<IdentityUserLogin<Guid>>());
        Configure(builder.Entity<IdentityUserToken<Guid>>());
        Configure(builder.Entity<IdentityUserClaim<Guid>>());
    }

}

