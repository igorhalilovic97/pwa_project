﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;

#pragma warning disable CS8765, CS8618
namespace PWA.Data;

public class Role : IdentityRole<Guid>
{
    private Role()
    { }

    public Role(string name)
    {
        Name = name;
    }

    public override string Name
    {
        get => base.Name!;
        set => base.Name = value ?? throw new NullReferenceException(nameof(Name));
    }
}
